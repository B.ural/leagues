package com.example.service;

import com.example.entity.League;
import com.example.repository.LeagueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Berat URAL
 * @since 6/15/2022
 */
@Service
public class LeagueService {

    @Autowired
    private LeagueRepository leagueRepository;

    public List<League> findAllLeague(){
        return leagueRepository.findAllLeague();
    }

    public void saveLeague(League league){
        leagueRepository.save(league);
    }

    public Optional<League> findLeagueById(Long id){
        return leagueRepository.findById(id);
    }

    private Optional<League> leagueId(Long id) {
        Optional<League> league = leagueRepository.findById(id);
        return league;
    }

    public String deleteLeagueById(Long id) {
        Optional<League> league = leagueId(id);
        leagueRepository.delete(league.get());
        return "Deleted";
    }
}
