package com.example.service;

import com.example.entity.Referee;
import com.example.entity.Referee;
import com.example.repository.RefereeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Berat URAL
 * @since 6/15/2022
 */
@Service
public class RefereeService {

    @Autowired
    private RefereeRepository refereeRepository;

    public List<Referee> findAllReferee(){
        return refereeRepository.findAll();
    }

    public void saveReferee(Referee referee){
        refereeRepository.save(referee);
    }
    
    public Optional<Referee> findRefereeById(Long id){
        return refereeRepository.findById(id);
    }

    private Optional<Referee> RefereeId(Long id) {
        Optional<Referee> referee = refereeRepository.findById(id);
        return referee;
    }

    public String deleteRefereeById(Long id) {
        Optional<Referee> referee = RefereeId(id);
        refereeRepository.delete(referee.get());
        return "Deleted";
    }
}
