package com.example.service;

import com.example.entity.TeamCaptain;
import com.example.entity.TeamCaptain;
import com.example.entity.Team;
import com.example.entity.TeamCaptain;
import com.example.repository.TeamCaptainRepository;
import com.example.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Berat URAL
 * @since 6/15/2022
 */
@Service
public class TeamCaptainService {

    @Autowired
    private TeamCaptainRepository teamCaptainRepository;

    public List<TeamCaptain> findAllTeamCaptain(){
        return teamCaptainRepository.findAllTeamCaptain();
    }

    public void saveTeamCaptain(TeamCaptain teamCaptain){
        teamCaptainRepository.save(teamCaptain);
    }

    public Optional<TeamCaptain> findTeamCaptainById(Long id){
        return teamCaptainRepository.findById(id);
    }


    private Optional<TeamCaptain> TeamCaptainId(Long id) {
        Optional<TeamCaptain> teamCaptain = teamCaptainRepository.findById(id);
        return teamCaptain;
    }

    public String deleteTeamCaptainById(Long id) {
        Optional<TeamCaptain> teamCaptain = TeamCaptainId(id);
        teamCaptainRepository.delete(teamCaptain.get());
        return "Deleted";
    }
    
}
