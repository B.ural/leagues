package com.example.service;

import com.example.entity.Team;
import com.example.entity.Team;
import com.example.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Berat URAL
 * @since 6/15/2022
 */
@Service
public class TeamService {

    @Autowired
    private TeamRepository teamRepository;

    public List<Team> findAllTeam(){
        return teamRepository.findAllTeam();
    }

    public void saveTeam(Team team){
        teamRepository.save(team);
    }

    public Optional<Team> findTeamById(Long id){
        return teamRepository.findById(id);
    }

    private Optional<Team> TeamId(Long id) {
        Optional<Team> Team = teamRepository.findById(id);
        return Team;
    }

    public String deleteTeamById(Long id) {
        Optional<Team> Team = TeamId(id);
        teamRepository.delete(Team.get());
        return "Deleted";
    }
    
}
