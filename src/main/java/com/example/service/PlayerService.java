package com.example.service;

import com.example.entity.Player;
import com.example.entity.Team;
import com.example.repository.PlayerRepository;
import com.example.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * @author Berat URAL
 * @since 6/15/2022
 */
@Service
public class PlayerService {

    @Autowired
    private PlayerRepository playerRepository;

    public List<Player> findAllPlayer(){
        return playerRepository.findAll();
    }

    public void savePlayer(Player Player){
        playerRepository.save(Player);
    }

    public void findPlayerByAdi(String adi){
        playerRepository.findPlayerByAdi(adi);
    }

    public void deletePlayerById(Long id){
        playerRepository.deleteById(id);
    }


}
