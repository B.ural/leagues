package com.example.service;

import com.example.entity.PlayerTeamHistory;
import com.example.repository.PlayerTeamHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * @author Berat URAL
 * @since 6/15/2022
 */
@Service
public class PlayerTeamHistoryService {

    @Autowired
    private PlayerTeamHistoryRepository playerTeamHistoryRepository;

    public List<PlayerTeamHistory> findAllPlayerTeamHistory(){
        return playerTeamHistoryRepository.findAll();
    }

    public void savePlayerTeamHistory(PlayerTeamHistory playerTeamHistory){
        playerTeamHistoryRepository.save(playerTeamHistory);
    }

    public void findPlayerTeamHistoryById(Long id){
        playerTeamHistoryRepository.findById(id);
    }

    public void deletePlayerTeamHistory(Long id){
        playerTeamHistoryRepository.deleteById(id);
    }

}
