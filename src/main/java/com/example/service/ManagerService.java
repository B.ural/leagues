package com.example.service;

import com.example.entity.Manager;
import com.example.entity.Manager;
import com.example.repository.ManagerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Berat URAL
 * @since 6/15/2022
 */
@Service
public class ManagerService {

    @Autowired
    private ManagerRepository managerRepository;

    public List<Manager> findAllManager(){
        return managerRepository.findAll();
    }

    public void saveManager(Manager manager){
        managerRepository.save(manager);
    }

    public Optional<Manager> findManagerById(Long id){
        return managerRepository.findById(id);
    }

    private Optional<Manager> ManagerId(Long id) {
        Optional<Manager> manager = managerRepository.findById(id);
        return manager;
    }

    public String deleteManagerById(Long id) {
        Optional<Manager> manager = ManagerId(id);
        managerRepository.delete(manager.get());
        return "Deleted";
    }
}
