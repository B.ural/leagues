package com.example.controller;

import com.example.entity.League;
import com.example.service.LeagueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class LeagueController {

    @Autowired
    LeagueService leagueService;

    @GetMapping(path = "/api/v2/findAllLeague")
    public List<League> findAllLeague() {
        List<League> leagueList = leagueService.findAllLeague();
        return leagueList;
    }

    @GetMapping(path = "/api/v2/saveLeague")
    public void saveProvince(@RequestBody League league) {
        leagueService.saveLeague(league);
    }

    @PostMapping(path = "/api/v2/findLeagueById")
    public Optional<League> findLeagueById(Long id){
        Optional<League> leagueList = leagueService.findLeagueById(id);
        return leagueList;
    }

    @PostMapping(path = "/api/v2/deleteLeagueById")
    public String deleteLeagueById(Long id){
        leagueService.deleteLeagueById(id);
        return "Deleted";
    }


}
