package com.example.controller;

import com.example.entity.TeamCaptain;
import com.example.service.TeamCaptainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class TeamCaptainController {

    @Autowired
    TeamCaptainService teamCaptainService;

    @GetMapping(path = "/api/v2/findAllTeamCaptain")
    public List<TeamCaptain> findAllTeamCaptain() {
        List<TeamCaptain> TeamCaptainList = teamCaptainService.findAllTeamCaptain();
        return TeamCaptainList;
    }

    @GetMapping(path = "/api/v2/saveTeamCaptain")
    public void saveProvince(@RequestBody TeamCaptain TeamCaptain) {
        teamCaptainService.saveTeamCaptain(TeamCaptain);
    }

    @PostMapping(path = "/api/v2/findTeamCaptainById")
    public Optional<TeamCaptain> findTeamCaptainById(Long id){
        Optional<TeamCaptain> TeamCaptainList = teamCaptainService.findTeamCaptainById(id);
        return TeamCaptainList;
    }

    @PostMapping(path = "/api/v2/deleteTeamCaptainById")
    public String deleteTeamCaptainById(Long id){
        teamCaptainService.deleteTeamCaptainById(id);
        return "Deleted";
    }


}
