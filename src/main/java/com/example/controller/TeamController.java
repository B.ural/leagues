package com.example.controller;

import com.example.entity.Team;
import com.example.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class TeamController {

    @Autowired
    TeamService teamService;

    @GetMapping( "/api/v2/findAllTeam")
    public List<Team> findAllTeam() {
        List<Team> TeamList = teamService.findAllTeam();
        return TeamList;
    }

    @GetMapping(path = "/api/v2/saveTeam")
    public void saveProvince(@RequestBody Team Team) {
        teamService.saveTeam(Team);
    }

    @PostMapping(path = "/api/v2/findTeamById")
    public Optional<Team> findTeamById(Long id){
        Optional<Team> TeamList =teamService.findTeamById(id);
        return TeamList;
    }

    @PostMapping(path = "/api/v2/deleteTeamById")
    public String deleteTeamById(Long id){
        teamService.deleteTeamById(id);
        return "Deleted";
    }


}
