package com.example.controller;

import com.example.entity.Manager;
import com.example.service.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class ManagerController {

    @Autowired
    ManagerService managerService;

    @GetMapping(path = "/api/v2/findAllManager")
    public List<Manager> findAllManager() {
        List<Manager> ManagerList = managerService.findAllManager();
        return ManagerList;
    }

    @GetMapping(path = "/api/v2/saveManager")
    public void saveProvince(@RequestBody Manager Manager) {
        managerService.saveManager(Manager);
    }

    @PostMapping(path = "/api/v2/findManagerById")
    public Optional<Manager> findManagerById(Long id){
        Optional<Manager> ManagerList = managerService.findManagerById(id);
        return ManagerList;
    }

    @PostMapping(path = "/api/v2/deleteManagerById")
    public String deleteManagerById(Long id){
        managerService.deleteManagerById(id);
        return "Deleted";
    }


}
