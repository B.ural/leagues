package com.example.controller;

import com.example.entity.PlayerTeamHistory;
import com.example.service.PlayerTeamHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class PlayerTeamHistoryController {

    @Autowired
    PlayerTeamHistoryService playerTeamHistoryService;

    @GetMapping(path = "/api/v2/findAllPlayerTeamHistory")
    public List<PlayerTeamHistory> findAllPlayerTeamHistory() {
        List<PlayerTeamHistory> PlayerTeamHistoryList = playerTeamHistoryService.findAllPlayerTeamHistory();
        return PlayerTeamHistoryList;
    }

    @GetMapping(path = "/api/v2/savePlayerTeamHistory")
    public void saveProvince(@RequestBody PlayerTeamHistory PlayerTeamHistory) {
        playerTeamHistoryService.savePlayerTeamHistory(PlayerTeamHistory);
    }

//    @PostMapping(path = "/api/v2/findPlayerTeamHistoryById")
//    public Optional<PlayerTeamHistory> findPlayerTeamHistoryById(Long id){
//        Optional<PlayerTeamHistory> PlayerTeamHistoryList = playerTeamHistoryService.findPlayerTeamHistoryById(id);
//        return PlayerTeamHistoryList;
//    }

    @PostMapping(path = "/api/v2/deletePlayerTeamHistoryById")
    public String deletePlayerTeamHistoryById(Long id){
        playerTeamHistoryService.deletePlayerTeamHistory(id);
        return "Deleted";
    }


}
