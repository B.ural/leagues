package com.example.controller;

import com.example.entity.Player;
import com.example.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class PlayerController {

    @Autowired
    PlayerService playerService;

    @GetMapping(path = "/api/v2/findAllPlayer")
    public List<Player> findAllPlayer() {
        List<Player> PlayerList = playerService.findAllPlayer();
        return PlayerList;
    }

    @GetMapping(path = "/api/v2/savePlayer")
    public void saveProvince(@RequestBody Player Player) {
        playerService.savePlayer(Player);
    }

    @PostMapping(path = "/api/v2/deletePlayerById")
    public String deletePlayerById(Long id){
        playerService.deletePlayerById(id);
        return "Deleted";
    }



}
