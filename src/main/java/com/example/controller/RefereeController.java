package com.example.controller;

import com.example.entity.Referee;
import com.example.service.RefereeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class RefereeController {

    @Autowired
    RefereeService refereeService;

    @GetMapping(path = "/api/v2/findAllReferee")
    public List<Referee> findAllReferee() {
        List<Referee> RefereeList = refereeService.findAllReferee();
        return RefereeList;
    }

    @GetMapping(path = "/api/v2/saveReferee")
    public void saveProvince(@RequestBody Referee Referee) {
        refereeService.saveReferee(Referee);
    }

    @PostMapping(path = "/api/v2/findRefereeById")
    public Optional<Referee> findRefereeById(Long id){
        Optional<Referee> RefereeList = refereeService.findRefereeById(id);
        return RefereeList;
    }

    @PostMapping(path = "/api/v2/deleteRefereeById")
    public String deleteRefereeById(Long id){
        refereeService.deleteRefereeById(id);
        return "Deleted";
    }


}
