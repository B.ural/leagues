package com.example.entity;

import javax.persistence.*;
/**
 * @author Berat URAL
 * @since 6/13/2022
 */
@Entity
@Table(name = "TEAM")
public class Team {

    @Id
    @SequenceGenerator(name = "generator" , sequenceName = "TEAM_ID_SEQ" , initialValue = 0, allocationSize = 1)
    @GeneratedValue(generator = "generator" , strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column
    private String adi;

    @Column
    private String president;


    @Column
    private Long kurulusTarihi;

    @Column
    private Long kupaSayisi;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PLAYER" , foreignKey = @ForeignKey(name = "ID_TEAM_PLAYER" , value = ConstraintMode.CONSTRAINT))
    private Player player;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_COACH" , foreignKey = @ForeignKey(name = "ID_TEAM_MANAGER" , value = ConstraintMode.CONSTRAINT))
    private Manager manager;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_TEAM_CAPTAIN" , foreignKey = @ForeignKey(name = "ID_TEAM_TEAM_CAPTAIN" , value = ConstraintMode.CONSTRAINT))
    private TeamCaptain teamCaptain;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAdi() {
        return adi;
    }

    public void setAdi(String adi) {
        this.adi = adi;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public Long getKurulusTarihi() {
        return kurulusTarihi;
    }

    public void setKurulusTarihi(Long kurulusTarihi) {
        this.kurulusTarihi = kurulusTarihi;
    }

    public Long getKupaSayisi() {
        return kupaSayisi;
    }

    public void setKupaSayisi(Long kupaSayisi) {
        this.kupaSayisi = kupaSayisi;
    }

    public String getPresident() {
        return president;
    }

    public void setPresident(String president) {
        this.president = president;
    }

    public TeamCaptain getTeamCaptain() {
        return teamCaptain;
    }

    public void setTeamCaptain(TeamCaptain teamCaptain) {
        this.teamCaptain = teamCaptain;
    }
}
