package com.example.entity;

import javax.persistence.*;
import java.sql.Ref;

/**
 * @author Berat URAL
 * @since 6/13/2022
 */
@Entity
@Table(name = "LEAGUE")
public class League {

    @Id
    @SequenceGenerator(name = "generator" , sequenceName = "LEAGUE_ID_SEQ" , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "generator" , strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column
    private String adi;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_TEAM" , foreignKey = @ForeignKey(name = "ID_LEAGUE_TEAM" , value = ConstraintMode.CONSTRAINT))
    private Team team;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_REFEREE" , foreignKey = @ForeignKey(name = "ID_LEAGUE_REFEREE" , value = ConstraintMode.CONSTRAINT))
    private Referee referee;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAdi() {
        return adi;
    }

    public void setAdi(String adi) {
        this.adi = adi;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Referee getReferee() {
        return referee;
    }

    public void setReferee(Referee referee) {
        this.referee = referee;
    }
}
