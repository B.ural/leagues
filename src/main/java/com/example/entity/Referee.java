package com.example.entity;

import javax.persistence.*;
/**
 * @author Berat URAL
 * @since 6/13/2022
 */
@Entity
@Table(name = "REFEREE")
public class Referee {

    @Id
    @SequenceGenerator(name = "generator" , sequenceName = "REFEREE_ID_SEQ" , initialValue = 0, allocationSize = 1)
    @GeneratedValue(generator = "generator" , strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column
    private String ad;

    @Column
    private String soyad;

    @Column
    private String uyruk;

    @Column
    private Long yas;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public String getSoyad() {
        return soyad;
    }

    public void setSoyad(String soyad) {
        this.soyad = soyad;
    }

    public String getUyruk() {
        return uyruk;
    }

    public void setUyruk(String uyruk) {
        this.uyruk = uyruk;
    }

    public Long getYas() {
        return yas;
    }

    public void setYas(Long yas) {
        this.yas = yas;
    }
}
