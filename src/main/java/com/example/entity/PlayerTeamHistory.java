package com.example.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Berat URAL
 * @since 6/13/2022
 */
@Entity
@Table(name = "PLAYER_TEAM_HISTORY")
public class PlayerTeamHistory {

    @Id
    @SequenceGenerator(name = "generator" , sequenceName = "PLAYER_TEAM_HISTORY_ID_SEQ" , initialValue = 0, allocationSize = 1)
    @GeneratedValue(generator = "generator" , strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PLAYER" , foreignKey = @ForeignKey(name = "ID_PLAYER_TEAM_HISTORY_PLAYER" , value = ConstraintMode.CONSTRAINT))
    private Player player;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_TEAM" , foreignKey = @ForeignKey(name = "ID_PLAYER_TEAM_HISTORY_TEAM" , value = ConstraintMode.CONSTRAINT))
    private Team team;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date sozlesmeBaslangic;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date sozlesmeBitis;

    @Column
    private Long piyasaDegeri;

    @Column
    private String mevki;

    @Column
    private Long numara;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Date getSozlesmeBaslangic() {
        return sozlesmeBaslangic;
    }

    public void setSozlesmeBaslangic(Date sozlesmeBaslangic) {
        this.sozlesmeBaslangic = sozlesmeBaslangic;
    }

    public Date getSozlesmeBitis() {
        return sozlesmeBitis;
    }

    public void setSozlesmeBitis(Date sozlesmeBitis) {
        this.sozlesmeBitis = sozlesmeBitis;
    }

    public Long getPiyasaDegeri() {
        return piyasaDegeri;
    }

    public void setPiyasaDegeri(Long piyasaDegeri) {
        this.piyasaDegeri = piyasaDegeri;
    }

    public String getMevki() {
        return mevki;
    }

    public void setMevki(String mevki) {
        this.mevki = mevki;
    }

    public Long getNumara() {
        return numara;
    }

    public void setNumara(Long numara) {
        this.numara = numara;
    }
}
