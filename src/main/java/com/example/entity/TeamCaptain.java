package com.example.entity;

import javax.persistence.*;

/**
 * @author Berat URAL
 * @since 6/14/2022
 */
@Entity
@Table(name = "TEAM_CAPTAIN")
public class TeamCaptain {

    @Id
    @SequenceGenerator(name = "generator" , sequenceName = "TEAM_CAPTAIN_ID_SEQ" , initialValue = 0, allocationSize = 1)
    @GeneratedValue(generator = "generator" , strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_TEAM" , foreignKey = @ForeignKey(name = "ID_TEAM_CAPTAIN_TEAM" , value = ConstraintMode.CONSTRAINT))
    private Team team;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PLAYER" , foreignKey = @ForeignKey(name = "ID_TEAM_CAPTAIN_PLAYER" , value = ConstraintMode.CONSTRAINT))
    private Player player;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
