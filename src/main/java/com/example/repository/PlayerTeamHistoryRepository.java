package com.example.repository;

import com.example.entity.PlayerTeamHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
/**
 * @author Berat URAL
 * @since 6/15/2022
 */
@Repository
public interface PlayerTeamHistoryRepository extends JpaRepository<PlayerTeamHistory,Long> {

    @Query("Select playerTeamHistory From PlayerTeamHistory playerTeamHistory left join fetch PlayerTeamHistory.team team" +
            "left join fetch PlayerTeamHistory.player player")
    public List<PlayerTeamHistory> findAllPlayerTeamHistory();


}
