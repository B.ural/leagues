package com.example.repository;

import com.example.entity.League;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
/**
 * @author Berat URAL
 * @since 6/15/2022
 */
@Repository
public interface LeagueRepository extends JpaRepository<League,Long> {

    @Query("Select league From League league left join fetch League.team team" +
            "left join fetch League .referee referee")
    public List<League> findAllLeague();

    @Query("Select league From League league Where league.adi like ?1%")
    List<League> findAllByAdi(String adi);
}
