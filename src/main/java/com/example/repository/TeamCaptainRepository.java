package com.example.repository;

import com.example.entity.TeamCaptain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
/**
 * @author Berat URAL
 * @since 6/15/2022
 */
 @Repository
public interface TeamCaptainRepository extends JpaRepository<TeamCaptain,Long> {

    @Query("Select teamCaptain From TeamCaptain teamCaptain left join fetch TeamCaptain.team team" +
            "left join fetch TeamCaptain.player player")
    public List<TeamCaptain> findAllTeamCaptain();

}
