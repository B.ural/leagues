package com.example.repository;

import com.example.entity.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Berat URAL
 * @since 6/15/2022
 */
@Repository
public interface PlayerRepository extends JpaRepository<Player,Long> {

    @Query("SELECT player FROM Player player WHERE player.ad LIKE CONCAT ('%' , :ad , '%')")
    List<Player> findPlayerByAdi(String ad);
}
