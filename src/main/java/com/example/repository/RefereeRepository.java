package com.example.repository;

import com.example.entity.Referee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Berat URAL
 * @since 6/15/2022
 */
@Repository
public interface RefereeRepository extends JpaRepository<Referee,Long> {
}
