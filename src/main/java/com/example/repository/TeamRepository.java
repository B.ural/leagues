package com.example.repository;

import com.example.entity.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Berat URAL
 * @since 6/15/2022
 */
@Repository
public interface TeamRepository extends JpaRepository<Team,Long> {

    @Query("Select team From Team team left join fetch Team.player player" +
            "left join fetch Team .manager manager" +
            "left join fetch Team.teamCaptain teamCaptain")
    public List<Team> findAllTeam();

}
