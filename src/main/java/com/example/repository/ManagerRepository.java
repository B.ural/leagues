package com.example.repository;

import com.example.entity.Manager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Berat URAL
 * @since 6/15/2022
 */
@Repository
public interface ManagerRepository extends JpaRepository<Manager,Long> {
}
